module example

/**
 * This file is part of iTasks-Electron.
 *
 * iTasks-Electron is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * iTasks-Electron is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with iTasks-Electron. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import iTasks => qualified :: Menu, SelectAll

import ABC.Interpreter.JavaScript
import ABC.Interpreter.JavaScript.Monad

import Electron.App
import Electron.Menu

Start w = serveElectron
	(\_ electron_opts opts ->
		( electron_opts
		, {opts & persistTasks=False}
		, []
		))
	mainApp
	w

mainApp :: Task ()
mainApp =
	set (?Just menu) applicationMenu @! () >-|
	createWindow
		{ defaultOptions
		& windowModifier = \win -> appJS (win .# "webContents" .# "openDevTools" .$! ())
		, task           = \_ -> viewInformation [] 42
		}
	@! ()
where
	menu =
		[ menuItem "File" <<@ SubMenu
			[ menuItem "Quit" <<@ Accelerator "CmdOrCtrl+Q" <<@ Quit
			]
		]
