# iTasks-Electron

This project uses [iTasks][] as a backend for [Electron][] to create
cross-platform native-like applications with [Clean][].

This project is still in very early stages. Not enough projects have been built
yet to get a clear picture of the combinators that would be useful to have.
Building an application with iTasks-Electron currently almost certainly means
contributing back to the library here, which you are very much invited to do.

For example projects, see:

- [M&#601;b&#817;aqq&#275;&#353;][MBQS]
- [SoccerFun][]

## Overview

The idea is as follows:

- The main JavaScript application starts an iTasks server in the background.

- The iTasks server runs an Electron task which communicates over a different
  TCP channel from the normal `onRequest` tasks. It uses the
  `ABC.Interpreter.JavaScript` interface to JavaScript to run JavaScript code
  on the main JavaScript process to interface with Electron. In this way it can
  for example open a [`BrowserWindow`][Electron-BrowserWindow].

- This `BrowserWindow` can open a normal `onRequest` task. In this way you are
  running a plain iTasks application, only in a custom application window
  rather than in the browser.

- Alternatively, the `BrowserWindow` can make use of Electron tasks and bypass
  the iTasks frontend completely. It then uses a straightforward interface to
  send messages to and from the server. The client again acts on those messages
  using the `ABC.Interpreter.JavaScript` foreign function interface.

- The two ways of using `BrowserWindow`s can be combined in the same
  application. Thus it is possible to use a fully customized main view, but use
  iTasks generic editors for option dialogs, for example.

## Installation

1. Install [Node.js][] for your platform. Make sure that `npm` is in your path.

2. Create a project and add `itasks-electron` as a [nitrile][] dependency:

	```bash
	git init my-project
	cd my-project
	nitrile init
	```

3. Your `nitrile.yml` should contain something like this:

	```yml
	dependencies:
	  itasks-electron: ^0.1

	src:
	  - src

	clm_options:
	  compiler: cocl-itasks
	  fusion: GenericFusion
	  bytecode: prelinked
	  generate_descriptors: true
	  export_local_labels: true
	  strip: false
	  post_link: web-resource-collector
	  heap: 128m
	  print_result: false
	  print_time: false

	build:
	  myproject:
	    script:
	      - clm:
	          main: myproject
	          target: myproject.exe
	```

	Replace `myproject` with the name of your project.

4. Optionally, create a `package.json` for `npm` using `npm init`:

	```bash
	npm init
	npm install --save-dev electron
	```

	(If you skip this step, the application will create a basic `package.json`
	and install `electron` on the first run.)

5. On linux you may need to fix permissions of the Chrome sandbox:

	```bash
	sudo chown root:root node_modules/electron/dist/chrome-sandbox
	sudo chmod 4755 node_modules/electron/dist/chrome-sandbox
	```

6. Create an `index.js` with the following contents:

	```js
	const process=require ('process');
	
	/* directory structure looks different for deployed applications */
	if (__dirname.substr (__dirname.length-14).match (/[\/\\]resources[\/\\]app/))
		process.chdir (require ('path').join (__dirname,'..','..'));
	else
		process.chdir (__dirname);
	
	const {run}=require ('./myproject-www/js/itasks-electron.js');
	run ({app: 'myproject'});
	```

	Replace `myproject` with the name of your project.

You are now ready to write and compile your project. Use `serveElectron` in
your `Start` rule, and build with `nitrile build`.

To run the application you only need to run the executable. You do not need to
run the JavaScript code yourself; this is done automatically by Clean.

For an example with Clean code, see the [examples](/examples).

## Distributing

To distribute your application you don't want your users to have to install
Node.js. It is possible to package Electron applications complete with a Chrome
runtime, so that you get a completely stand-alone application.

You need to run `npm install --save-dev electron-packager copy-dir` to install
the necessary dependencies to be able to package applications.

After that, you need to write a `build.js` using iTasks-Electron's
`itasks-electron-packager.js`. A simple `build.js` looks like this:

```js
const path=require ('path');
const packager=require ('./nitrile-packages/linux-x64/itasks-electron/tools/itasks-electron-packager.js');

packager ({
	out: path.join ('..','dist'),
	cleanApp: 'myproject',
	electronApp: 'ApplicationName'
}).then (appPaths => {
	console.log (`Electron app bundles created:\n${appPaths.join('\n')}`);
});
```

This assumes that the Clean application (excluding `.exe`) is called
`myproject`. The application is put in `../dist/<name>-<platform>-<arch>`, and
the main executable is `ApplicationName` (with the appropriate extension
depending on the operating system).

In more advanced cases you may want to ignore certain files from the
distribution, or add files in the build step. See the build script of
M&#601;b&#817;aqq&#275;&#353; for a more advanced configuration:
https://gitlab.com/camilstaps/mbqs/-/blob/master/src/build.js

## Author &amp; License
This library is maintained by [Camil Staps][].

Copyright is owned by the authors of individual commits.

This project is licensed under AGPL v3 with additional terms under section 7;
see the [LICENSE](/LICENSE) file for details.

[Camil Staps]: https://camilstaps.nl
[Clean]: https://clean-lang.org
[Electron]: https://www.electronjs.org/
[Electron-BrowserWindow]: https://www.electronjs.org/docs/api/browser-window
[electron-packager]: https://www.npmjs.com/package/electron-packager
[iTasks]: https://clean-lang.org/pkg/itasks
[Node.js]: https://nodejs.org/en/
[SoccerFun]: https://gitlab.com/camilstaps/soccerfun
[MBQS]: https://gitlab.com/camilstaps/mbqs/
