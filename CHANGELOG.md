# Changelog

#### v0.1.5

- Chore: update to itasks 0.8.

#### v0.1.4

- Chore: accept iTasks 0.2–0.7 as dependency.

#### v0.1.3

- Feature: add `getBrowserWindow`.

#### v0.1.2

- Chore: move to https://clean-lang.org.

#### v0.1.1

- Feature: add `loadURL :: !String -> Task ()`.

### v0.1

First tagged release.
