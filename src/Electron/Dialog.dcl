definition module Electron.Dialog

/**
 * This file is part of iTasks-Electron.
 *
 * iTasks-Electron is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * iTasks-Electron is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with iTasks-Electron. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

from System.FilePath import :: FilePath

from iTasks.WF.Definition import :: Task

:: MessageType
	= InfoMsg
	| ErrorMsg
	| QuestionMsg
	| WarningMsg

/**
 * Shows a dialog and returns the selected answer, if any.
 * @param The message type (info, error, question, or warning).
 * @param The title for the dialog.
 * @param The message itself.
 * @param A list of possible answers.
 * @param The index of the default answer int he list.
 * @result The selected answer, or `?None` if the dialog was closed without
 *   selecting an answer (e.g. with Escape).
 */
showMessageBox :: !MessageType !String !String ![String] !Int -> Task (?String)

showSimpleMessageBox :: !MessageType !String -> Task ()

:: OpenDialogSettings =
	{ allowFiles       :: !Bool //* Default true.
	, allowDirectories :: !Bool //* Default false.
	, showHiddenFiles  :: !Bool //* Default false.
	, multiSelections  :: !Bool //* Default false.
	, filters          :: ![(String, [String])] //* A list of file type names and associated extensions.
	, defaultPath      :: !?FilePath
	}

defaultOpenDialogSettings :: OpenDialogSettings

showOpenDialog :: !String !OpenDialogSettings -> Task (?String)

:: SaveDialogSettings =
	{ showHiddenFiles :: !Bool //* Default false.
	, filters         :: ![(String, [String])] //* A list of file type names and associated extensions.
	, defaultPath     :: !?FilePath
	}

defaultSaveDialogSettings :: SaveDialogSettings

showSaveDialog :: !String !SaveDialogSettings -> Task (?String)
