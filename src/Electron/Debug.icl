implementation module Electron.Debug

/**
 * This file is part of iTasks-Electron.
 *
 * iTasks-Electron is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * iTasks-Electron is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with iTasks-Electron. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import StdEnv

import qualified Data.Map

import graph_copy

import iTasks
import iTasks.Internal.IWorld
import iTasks.Internal.Task

:: DebugInfo =
	{ memory_shares :: !ShareSizes
	}

:: ShareSizes =
	{ total_size :: !Int
	, elements   :: ![(String,Int)]
	}

derive class iTask DebugInfo, ShareSizes

showDebugInfo :: Task ()
showDebugInfo = getInfo >>- \info -> viewInformation [] info @! ()
where
	getInfo :: Task DebugInfo
	getInfo = mkInstantTask \_ iworld
		# (info,iworld) = getInfo iworld
		-> (Ok info, iworld)
	where
		getInfo :: !*IWorld -> (!DebugInfo, !*IWorld)
		getInfo iworld=:{memoryShares} =
			(
				{ memory_shares = shareSizes memoryShares
				}
			, iworld
			)

		shareSizes :: !('Data.Map'.Map String a) -> ShareSizes
		shareSizes shares
			# elements = [(name, fst (usize (copy_to_string val))) \\ (name,val) <- 'Data.Map'.toList shares]
			=
				{ total_size = sum (map snd elements)
				, elements   = elements
				}
