implementation module Electron.App

/**
 * This file is part of iTasks-Electron.
 *
 * iTasks-Electron is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * iTasks-Electron is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with iTasks-Electron. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import StdEnv

import Control.Monad => qualified return, forever, sequence
from Data.Func import $, hyperstrict, seqSt
import Data.Functor
import Data.GenHash
import Data.List => qualified group
import qualified Data.Map
from Data.Map import :: Map, instance Functor (Map k)
import Data.Map.GenJSON
import Data.Tuple
import System.CommandLine
import System.File
import System.FilePath
import System.Process
import System._Unsafe
import qualified Text
from Text import class Text, instance Text String, concat3

import graph_copy

import TCPIP => qualified :: Port

import ABC.Interpreter
import ABC.Interpreter.JavaScript
import ABC.Interpreter.JavaScript.Monad

import iTasks => qualified forever
import iTasks.Internal.IWorld
import iTasks.Internal.Task
import iTasks.UI.JavaScript

import Electron.Debug
from Electron.Menu import watchApplicationMenu

derive class iTask ElectronWindow
derive gHash ElectronWindow

instance == ElectronWindow where (==) (ElectronWindow x) (ElectronWindow y) = x == y
instance < ElectronWindow where (<) (ElectronWindow x) (ElectronWindow y) = x < y

derive JSONEncode Symbol, PrelinkedInterpretationEnvironment
JSONEncode{|{#Symbol}|} b xs = JSONEncode{|*|} b [x \\ x <-: xs]
JSONEncode{|{#Int}|}    b xs = JSONEncode{|*|} b [x \\ x <-: xs]

derive JSONDecode Symbol, PrelinkedInterpretationEnvironment
JSONDecode{|{#Symbol}|} b json = appFst (fmap toArray) (JSONDecode{|*|} b json)
JSONDecode{|{#Int}|}    b json = appFst (fmap toArray) (JSONDecode{|*|} b json)

toArray :: [a] -> {#a} | Array {#} a
toArray xs = {#x \\ x <- xs}

:: TCPLocal =
	{ buffer :: !String
	}

:: TCPMsg =
	{ type  :: !String
	, val   :: !String
	, extra :: ![(String, JSONNode)]
	}

:: TCPShare =
	{ tcpRx          :: ![TCPMsg]
	, tcpTx          :: ![TCPMsg]
	, handlers       :: !'Data.Map'.Map Int (String -> Task ())
	, handlerCounter :: !Int
	}

derive class iTask TCPLocal, TCPShare

JSONEncode{|TCPMsg|} _ {type,val,extra} = [JSONObject [("type",JSONString type),("val",JSONString val):extra]]
JSONDecode{|TCPMsg|} _ [JSONObject obj:json] =
	case (lookup "type" obj, lookup "val" obj) of
		(?Just (JSONString type), ?Just (JSONString val)) ->
			(?Just {type=type, val=val, extra=[kv \\ kv=:(k,_) <- obj | k <> "type" && k <> "val"]}, json)
		_ ->
			(?None, json)
JSONDecode{|TCPMsg|} _ json = (?None, json)

derive gEq TCPMsg
derive gText TCPMsg
derive gEditor TCPMsg

tcpShare :: SimpleSDSLens TCPShare
tcpShare =: sdsFocus "tcpShare" (memoryStore "Electron" (?Just
	{ tcpRx          = []
	, tcpTx          = []
	, handlers       = 'Data.Map'.newMap
	, handlerCounter = 0
	}))

tcpRx :: SimpleSDSLens [TCPMsg]
tcpRx =: mapReadWrite (\s -> s.tcpRx, \rx s -> ?Just {s & tcpRx=rx}) ?None tcpShare

tcpQueueEmpty :: SimpleSDSLens Bool
tcpQueueEmpty =: mapReadWrite
	( \{tcpTx} -> isEmpty tcpTx
	, \_ _ -> ?None
	)
	?None
	tcpShare

addHandler :: !(String -> Task ()) -> Task Int
addHandler t =
	upd
		(\shr -> let i = shr.handlerCounter + 1 in
			{ shr
			& handlers       = 'Data.Map'.put i t shr.TCPShare.handlers
			, handlerCounter = i
			})
		tcpShare @
	\shr -> shr.handlerCounter

serveElectron ::
	!([String] ElectronOptions EngineOptions -> (ElectronOptions, EngineOptions, [StartableTask]))
	!(Task a)
	!*World -> *World
	| iTask a
serveElectron modOpts startConnection w
	# ([_:args],w) = getCommandLine w
	| isEmpty args
		# w = maybeInstall w
		= justRunProcess electron ["."] w
	# action = hd args
	| action == "npm"
		= justRunProcess "npm" (tl args) w
	| action == "serve"
		= serve modOpts startConnection w
	| otherwise
		= abort ("Unknown action '"+++action+++"'\n")
where
	electron = concatPaths ["node_modules","electron","dist","electron"]

	maybeInstall :: !*World -> *World
	maybeInstall w
		# (exi,w) = fileExists electron w
		| exi = w
		# (exi,w) = fileExists "package.json" w
		| not exi = abort "package.json does not exist\n"
		# (_,w) = fclose (stderr <<< "electron binary not found; running npm install...\n") w
		= justRunProcess "npm" ["install"] w

	justRunProcess :: !FilePath ![String] !*World -> *World
	justRunProcess cmd args w
		# (rcode,w) = callProcess cmd args ?None w
		# rcode = if (isError rcode) 1 (fromOk rcode)
		= setReturnCode rcode w

serve ::
	!([String] ElectronOptions EngineOptions -> (ElectronOptions, EngineOptions, [StartableTask]))
	!(Task a)
	!*World -> *World
	| iTask a
serve modOpts startConnection w = doTasksWithOptions
	(\[arg0,_:argv] opts
		# electron_opts =
			{ port  = 0
			, debug = False
			}
		# (cli,electron_opts,opts) = parseBaseOptions argv electron_opts opts
		# mbOptions = defaultEngineCLIOptions () [arg0:cli] opts
		| isError mbOptions
			-> liftError mbOptions
		# opts = snd (fromOk mbOptions)
		# (electron_opts,opts,startup) = modOpts cli electron_opts opts
		# main =
			[ onStartup (task electron_opts.ElectronOptions.port)
			, onStartup watchApplicationMenu
			, onRequest "/electron" (return ())
			: if electron_opts.debug
				[onRequest "/debug" showDebugInfo]
				[]
			]
		-> Ok (startup ++ main, opts))
	w
where
	task :: !Int -> Task ()
	task port =
		getPIE >>- \pie ->
		(connect port pie -&&- loop pie) @!
		()

	connect :: !Int !PrelinkedInterpretationEnvironment -> Task ()
	connect port pie = tcplisten (Port port) True tcpShare
		{ ConnectionHandlers
		| onConnect     = onConnect
		, onData        = onData
		, onShareChange = onShareChange
		, onDisconnect  = onDisconnect
		, onDestroy     = \local -> (Ok local, [])
		} @! ()
	where
		onConnect :: !ConnectionId !String !TCPShare -> (!MaybeErrorString TCPLocal, !?TCPShare, ![String], !Bool)
		onConnect _ _ _ = (Ok {buffer=""}, ?None, [], False)

		onData :: !ConnectionId !String !TCPLocal !TCPShare -> (!MaybeErrorString TCPLocal, !?TCPShare, ![String], !Bool)
		onData _ data local=:{buffer} share=:{tcpRx}
			# buffer = buffer +++ data
			# lines = 'Text'.split "\n" buffer
			# local & buffer = last lines
			# lines = map (fromJSON o fromString) (init lines)
			| isEmpty lines
				= (Ok local, ?None, [], False)
			| any isNone lines
				# json = toString (toJSON {type="err", val="invalid json", extra=[]}) +++ "\n"
				= (Ok local, ?Just share, [json], False)
			# share & tcpRx = tcpRx ++ [l \\ ?Just l <- lines]
			= (Ok local, ?Just share, [], False)

		onShareChange :: !TCPLocal !TCPShare -> (!MaybeErrorString TCPLocal, !?TCPShare, ![String], !Bool)
		onShareChange local share=:{tcpTx}
			| isEmpty tcpTx
				= (Ok local, ?None, [], False)
				# share & tcpTx = []
				= (Ok local, ?Just share, [toString (toJSON msg)+++"\n" \\ msg <- tcpTx], False)

		onDisconnect :: !ConnectionId !TCPLocal !TCPShare -> (!MaybeErrorString TCPLocal, !?TCPShare)
		onDisconnect _ local share=:{tcpTx,tcpRx} =
			( Ok local
			, ?Just
				{ share
				& tcpRx = []
				, tcpTx = []
				}
			)

	loop :: !PrelinkedInterpretationEnvironment -> Task ()
	loop pie =
		watch tcpRx >>*
		[ OnValue (ifValue (not o isEmpty) return)
		] >>- \rx ->
		set [] tcpRx >-| // TODO: atomic access; iTasks-SDK#370
		sequence [handle msg \\ msg <- rx] ||-
		loop pie
	where
		handle :: !TCPMsg -> Task ()
		handle msg = case msg.TCPMsg.type of
			"new" ->
				startConnection @! ()
			"close" ->
				sendTCPMsg {type="close",val="",extra=[]}
			"run"
				# (task,_) = jsDeserializeFromClient msg.val pie
				-> is_task task
			"result" ->
				case lookup "handler" msg.extra of
					?Just (JSONInt h) ->
						get tcpShare >>- \{TCPShare|handlers} ->
						upd (\shr -> {TCPShare | shr & handlers='Data.Map'.del h shr.TCPShare.handlers}) tcpShare >-|
						case 'Data.Map'.get h handlers of
							?None ->
								throw "no handler"
							?Just h ->
								h msg.val
					_ ->
						throw "no handler"
			"focus-window" ->
				case lookup "id" msg.extra of
					?Just (JSONInt id) ->
						set (?Just (ElectronWindow id)) currentWindow @! ()
					_ ->
						throw "invalid focus-window message"
			"blur-window" ->
				case lookup "id" msg.extra of
					?Just (JSONInt id) ->
						upd (\mbWin -> case mbWin of
							?Just (ElectronWindow win) | win == id -> ?None
							_                                      -> mbWin) currentWindow @! ()
					_ ->
						throw "invalid blur-window message"
			"close-window" ->
				case lookup "id" msg.extra of
					?Just (JSONInt id) ->
						upd (filter ((<>) (ElectronWindow id))) allWindows @! ()
					_ ->
						throw "invalid close-window message"
			_ ->
				throw ("invalid message type "+++msg.TCPMsg.type)
		where
			is_task :: !(Task ()) -> Task ()
			is_task t = t

getPIE :: Task PrelinkedInterpretationEnvironment
getPIE = mkInstantTask \_ iworld=:{IWorld | abcInterpreterEnv} -> (Ok abcInterpreterEnv,iworld)

parseBaseOptions :: ![String] !ElectronOptions !EngineOptions -> (![String], !ElectronOptions, !EngineOptions)
parseBaseOptions ["--electron-port":p:rest] electron_opts opts =
	parseBaseOptions rest {ElectronOptions | electron_opts & port=toInt p} opts
parseBaseOptions cli electron_opts opts = (cli,electron_opts,opts)

windowCounter :: SimpleSDSLens Int
windowCounter =: sdsFocus "windowCounter" (memoryStore "electron" (?Just 0))

defaultOptions :: ElectronWindowOptions a
defaultOptions =
	{ task           = \_ -> throw "No task set in ElectronWindowOptions"
	, webPreferences = []
	, windowModifier = pure
	, parent         = ?None
	, width          = 800
	, height         = 600
	}

allWindows :: SimpleSDSLens [ElectronWindow]
allWindows =: sdsFocus "allWindows" (memoryStore "electron" (?Just []))

currentWindow :: SimpleSDSLens (?ElectronWindow)
currentWindow =: sdsFocus "currentWindow" (memoryStore "electron" (?Just ?None))

createWindow :: !(ElectronWindowOptions a) -> Task ElectronWindow | iTask a
createWindow {task,webPreferences,windowModifier,parent,width,height} =
	upd inc windowCounter >>- \id ->
	let wid = ElectronWindow id in
	appendTopLevelTask 'Data.Map'.newMap False (task wid) >>- \(TaskId instanceNo _) ->
	get (sdsFocus instanceNo taskInstanceByNo) >>- \{TaskInstance|instanceKey=(?Just key)} ->
	get applicationURL >>- \serverUrl ->
	runInMainProcess (\comp w -> snd (runJS () comp (startup serverUrl parent instanceNo key id) w)) >-|
	upd ((++) [wid]) allWindows @!
	wid
where
	startup serverUrl mbParent instanceNo instanceKey id =
		gets (\st -> st.component) >>= \me ->
		accJS (case mbParent of
			?None ->
				\w -> (?None, w)
			?Just (ElectronWindow p) ->
				\w
					# (win,w) = (me .# "windows.get" .$ p) w
					-> (?Just win,w)) >>= \mbParent ->
		accJS (jsGlobal "require" .$ "electron") >>= \electron ->
		accJS (jsNew (electron .# "BrowserWindow") (jsRecord
			[ "width" :> width
			, "height" :> height
			, "webPreferences" :> jsRecord
				[ "nodeIntegration" :> True
				, "contextIsolation" :> False
				: webPreferences
				]
			: case mbParent of
				?None ->
					[]
				?Just win ->
					[ "modal" :> True
					, "parent" :> win
					]
			])) >>= \win ->
		mbHideMenu win >>|
		syncEvent "focus" me win id >>|
		syncEvent "blur" me win id >>|
		syncEvent "close" me win id >>|
		windowModifier win >>|
		appJS (win .# "itasks-id" .= id) >>|
		appJS (me .# "windows.set" .$! (id, win)) >>|
		accJS (win .# "loadURL" .$ (serverUrl+++"electron")) `then` \_ ->
		appJS (win .# "webContents" .# "send" .$! ("connect",instanceNo,instanceKey))
	mbHideMenu win
		| isNone parent =
			pure jsNull
		| otherwise =
			appJS (win .# "autoHideMenuBar" .= True) >>|
			appJS (win .# "setMenuBarVisibility" .$! False)

	syncEvent ev me win id =
		jsWrapMonad (\_ -> appJS (me .# "send" .$! (ev+++"-window", "", jsRecord ["id" :> id]))) >>= \handler ->
		appJS (win .# "on" .$! (ev, handler))

getBrowserWindow :: !ElectronWindow !*JSWorld -> (!?JSVal, !*JSWorld)
getBrowserWindow (ElectronWindow target) w
	# (electron,w) = (jsGlobal "require" .$ "electron") w
	# (wins,w) = (electron .# "BrowserWindow.getAllWindows" .$ ()) w
	# (wins,w) = jsValToList` wins id w
	= find wins w
where
	find [] w
		= (?None, w)
	find [win:wins] w
		# (id,w) = win .# "itasks-id" .? w
		| jsValToMaybe id == ?Just target
			= (?Just win, w)
			= find wins w

closeWindow :: Task ()
closeWindow = updateInformation [UpdateUsing id (flip const) editor] ()
where
	editor :: Editor () (EditorReport ())
	editor = JavaScriptInit initUI @>> leafEditorToEditor
		{ LeafEditor
		| onReset    = onReset
		, onEdit     = onEdit
		, onRefresh  = onRefresh
		, writeValue = writeValue
		}
	where
		onReset attributes mode vst = (Ok (uia UITextView attributes,(),?None), vst)
		onEdit () st vst = (Ok (NoChange,st,?None), vst)
		onRefresh _ st vst = (Ok (NoChange,st,?None), vst)
		writeValue s = Ok (ValidEditor s)
		initUI _ me world = (jsWindow .# "close" .$! ()) world

closeAllWindows :: Task ()
closeAllWindows =
	runInMainProcess \me world
		# (electron,world) = (jsGlobal "require" .$ "electron") world
		# (windows,world) = (electron .# "BrowserWindow" .# "getAllWindows" .$ ()) world
		# (windows,world) = jsValToList` windows id world
		-> seqSt (\win -> win .# "close" .$! ()) windows world

loadURL :: String -> Task ()
loadURL url = updateInformation [UpdateUsing id (flip const) editor] ()
where
	editor :: Editor () (EditorReport ())
	editor = JavaScriptInit initUI @>> leafEditorToEditor
		{ LeafEditor
		| onReset    = onReset
		, onEdit     = onEdit
		, onRefresh  = onRefresh
		, writeValue = writeValue
		}
	where
		onReset attributes mode vst = (Ok (uia UITextView attributes,(),?None), vst)
		onEdit () st vst = (Ok (NoChange,st,?None), vst)
		onRefresh _ st vst = (Ok (NoChange,st,?None), vst)
		writeValue s = Ok (ValidEditor s)
		initUI _ me world = (jsWindow .# "location" .= url) world

runInMainProcess :: !(JSVal *JSWorld -> *JSWorld) -> Task ()
runInMainProcess f = sendSerializedTCPMsg "run" [] (wrapInitFunction f)

runInMainProcessWithResult :: !(JSVal *JSWorld -> *(a, *JSWorld)) -> Task a | iTask a
runInMainProcessWithResult f =
	withShared ?None \result ->
	addHandler
		(\r ->
			getPIE >>- \pie
				# (r,ok) = jsDeserializeFromClient r pie
				| ok <> 0
					-> throw "deserialization failed\n"
					-> set (?Just r) result @! ()) >>- \h ->
	sendSerializedTCPMsg "run" [("handler",JSONInt h)] (toargf f h) >-|
	wait isJust result @
	fromJust
where
	toargf :: !(JSVal *JSWorld -> *(a, *JSWorld)) !Int !{!JSVal} !*JSWorld -> *JSWorld
	toargf f h args w
		# (r,w) = f args.[0] w
		#! r = hyperstrict r
		# (r,w) = jsSerializeOnClient r w
		# w = (args.[0] .# "send" .$! ("result", r, jsRecord ["handler" :> h])) w
		= w

sendSerializedTCPMsg :: !String ![(String, JSONNode)] a -> Task ()
sendSerializedTCPMsg type extra val =
	serialize val >>- \serialized ->
	sendTCPMsg {type=type, val=serialized, extra=extra}

sendTCPMsg :: !TCPMsg -> Task ()
sendTCPMsg msg =
	upd (\share=:{tcpTx} -> {share & tcpTx=tcpTx++[msg]}) tcpShare @!
	()

serialize :: a -> Task String
serialize graph = mkInstantTask
	\id iworld=:{IWorld | abcInterpreterEnv}
		# serialized = jsSerializeGraph graph abcInterpreterEnv
		-> (Ok serialized, iworld)
