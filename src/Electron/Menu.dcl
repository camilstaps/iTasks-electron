definition module Electron.Menu

/**
 * This file is part of iTasks-Electron.
 *
 * iTasks-Electron is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * iTasks-Electron is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with iTasks-Electron. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

from Data.Map import :: Map

from ABC.Interpreter.JavaScript import :: JSVal, generic gToJS

from iTasks.SDS.Definition import :: SDSLens, :: SimpleSDSLens
from iTasks.UI.Tune import class tune
from iTasks.WF.Definition import :: Task

from Electron.App import :: ElectronWindow

applicationMenu :: SimpleSDSLens (?Menu)

watchApplicationMenu :: Task ()

:: Menu :== [MenuItem]

:: MenuItem
	= MenuSeperator
	| MenuItem !MenuItemR

:: MenuItemR =
	{ label       :: !String
	, type        :: !MenuItemType
	, role        :: !?MenuItemRole
	, id          :: !?Int
	, accelerator :: !?String
	, enabled     :: !?Bool
	, submenu     :: !?Menu
	, onclick     :: !?(ElectronWindow -> Task ())
	}

menuItem :: !String -> MenuItem

derive gToJS MenuItem

:: SubMenu =: SubMenu Menu
instance tune SubMenu MenuItem

:: MenuItemRole
	= Undo | Redo
	| Cut | Copy | Paste | PasteAndMatchStyle | Delete
	| SelectAll
	| Reload | ForceReload
	| ToggleDevTools
	| ResetZoom | ZoomIn | ZoomOut | Zoom
	| ToggleFullScreen
	| Window | Minimize | Close | Hide | HideOthers | Unhide | Quit | Front
	| Help | About | Services
	| StartSpeaking | StopSpeaking
	| AppMenu | FileMenu | EditMenu | ViewMenu | WindowMenu
	| RecentDocuments | ClearRecentDocuments
	| ToggleTabBar | SelectNextTab | SelectPreviousTab
	| MergeAllWindows | MoveTabToNewWindow
instance tune MenuItemRole MenuItem

:: MenuAccelerator =: Accelerator String
instance tune MenuAccelerator MenuItem

:: EnabledDisabled = Enabled | Disabled
instance tune EnabledDisabled MenuItem

:: MenuItemType = NormalMenuItem | Checkbox | Radio
instance tune MenuItemType MenuItem

:: OnClickHandler =: OnClick (ElectronWindow -> Task ())
instance tune OnClickHandler MenuItem
