implementation module Electron.Menu

/**
 * This file is part of iTasks-Electron.
 *
 * iTasks-Electron is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * iTasks-Electron is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with iTasks-Electron. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import StdEnv

import Control.Applicative
import Control.Monad => qualified return, forever, sequence
import Data.Func
import Data.Functor
import Data.GenCons
import qualified Data.Map
from Data.Map import :: Map
import Data.Tuple
import System._Unsafe

import ABC.Interpreter.JavaScript
import ABC.Interpreter.JavaScript.Monad

import iTasks => qualified :: Menu, SelectAll

import Electron.App

menuItem :: !String -> MenuItem
menuItem label = MenuItem
	{ label       = label
	, type        = NormalMenuItem
	, role        = ?None
	, id          = ?None
	, accelerator = ?None
	, enabled     = ?None
	, submenu     = ?None
	, onclick     = ?None
	}

derive class iTask MenuItem, MenuItemR, MenuItemRole, MenuItemType

JSONEncode{|JSWorld|} _ _ = abort "JSONEncode{|JSWorld|}\n"
JSONDecode{|JSWorld|} _ _ = abort "JSONDecode{|JSWorld|}\n"
gEq{|JSWorld|} _ _ = abort "gEq{|JSWorld|}\n"
gText{|JSWorld|} _ _ = abort "gText{|JSWorld|}\n"
gEditor{|JSWorld|} _ = abort "gEditor{|JSWorld|}\n"

JSONEncode{|JSVal|} _ _ = abort "JSONEncode{|JSVal|}\n"
JSONDecode{|JSVal|} _ _ = abort "JSONDecode{|JSVal|}\n"
gEq{|JSVal|} _ _ = abort "gEq{|JSVal|}\n"
gText{|JSVal|} _ _ = abort "gText{|JSVal|}\n"
gEditor{|JSVal|} _ = abort "gEditor{|JSVal|}\n"

gToJS{|MenuItem|} MenuSeperator = toJS (jsRecord ["type" :> "separator"])
gToJS{|MenuItem|} (MenuItem i) = toJS i

derive consName MenuItemRole
gToJS{|MenuItemR|} item = toJS (jsRecord
	[ "label"       :> item.MenuItemR.label
	, "type"        :> item.MenuItemR.type
	, "role"        :> (\r -> let name = consName{|*|} r in name:=(0,toLower name.[0])) <$> item.role
	, "id"          :> item.MenuItemR.id
	, "accelerator" :> item.accelerator
	, "enabled"     :> item.enabled <> ?Just False
	, "submenu"     :> item.submenu
	])

gToJS{|MenuItemType|} type = case type of
	NormalMenuItem -> jsNull
	Checkbox       -> toJS "checkbox"
	Radio          -> toJS "radio"

applicationMenu :: SimpleSDSLens (?Menu)
applicationMenu =: mapReadWrite
	( fst
	, \menu (_,stamp) -> ?Just (menu,stamp+1)
	)
	?None
	stampedApplicationMenu

stampedApplicationMenu :: SimpleSDSLens (?Menu, Int)
stampedApplicationMenu =: sdsFocus "stampedApplicationmenu" (memoryStore "electron" (?Just (?None,0)))

watchApplicationMenu :: Task ()
watchApplicationMenu =
	get applicationMenu >>- \menu ->
	runInMainProcess (setMenu (addIds 0 <$> menu)) >-|
	get stampedApplicationMenu @ snd >>- \stamp ->
	wait ((<>) stamp o snd) stampedApplicationMenu >-|
	watchApplicationMenu
where
	setMenu mbMenu me world
		# (electron,world) = (jsGlobal "require" .$ "electron") world
		| isNone mbMenu
			= (electron .# "Menu" .# "setApplicationMenu" .$! jsNull) world
		# (menu,nids) = fromJust mbMenu
		# (emenu,world) = (electron .# "Menu" .# "buildFromTemplate" .$ menu) world
		# world = seqSt
			(\id world
				# onclick = findOnClickById id menu
				= case onclick of
					?None
						-> world
					?Just t
						# (onclick,world) = jsWrapFun
							(\args world
								# (id,world) = args.[1] .# "itasks-id" .?? (-1, world)
								# (t,world) = jsSerializeOnClient (t (ElectronWindow id)) world
								-> (me .# "runOnServer" .$! t) world)
							me world
						-> (jsCall (emenu .# "getMenuItemById") id .# "click" .= onclick) world)
			[i \\ i <- [0..nids-1]]
			world
		= (electron .# "Menu" .# "setApplicationMenu" .$! emenu) world
	addIds i menu
		= mapSt addIds` menu i
	where
		addIds` MenuSeperator i
			= (MenuSeperator,i)
		addIds` (MenuItem item) i
			# item & MenuItemR.id = ?Just i
			= case item.submenu of
				?None
					-> (MenuItem item,i+1)
				?Just m
					# (m,i) = addIds (i+1) m
					-> (MenuItem {item & submenu= ?Just m},i)
	findOnClickById i menu
		= foldl (<|>) ?None [find r <|> (findOnClickById i =<< r.submenu) \\ MenuItem r <- menu]
	where
		find item
			| isJust item.MenuItemR.id && fromJust item.MenuItemR.id == i
				= item.onclick
				= ?None

instance tune SubMenu MenuItem
where
	tune (SubMenu menu) (MenuItem r) = MenuItem {r & submenu= ?Just menu}

instance tune MenuItemRole MenuItem
where
	tune role (MenuItem r) = MenuItem {r & role= ?Just role}

instance tune MenuAccelerator MenuItem
where
	tune (Accelerator a) (MenuItem r) = MenuItem {r & accelerator= ?Just a}

instance tune EnabledDisabled MenuItem
where
	tune eord (MenuItem r) = MenuItem {r & enabled= ?Just (eord=:Enabled)}

instance tune MenuItemType MenuItem
where
	tune type (MenuItem r) = MenuItem {MenuItemR | r & type=type}

instance tune OnClickHandler MenuItem
where
	tune (OnClick h) (MenuItem r) = MenuItem {r & onclick= ?Just h}
