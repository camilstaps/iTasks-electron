implementation module Electron.Util

/**
 * This file is part of iTasks-Electron.
 *
 * iTasks-Electron is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * iTasks-Electron is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with iTasks-Electron. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import iTasks

import Electron.App

import ABC.Interpreter.JavaScript

getVersion :: Task String
getVersion = runInMainProcessWithResult
	\_ w
		# (electron,w) = (jsGlobal "require" .$ "electron") w
		-> (electron .# "app.getVersion" .$? ()) ("", w)

getElectronCommandLine :: Task [String]
getElectronCommandLine = runInMainProcessWithResult
	\_ w
		# (electron,w) = (jsGlobal "require" .$ "electron") w
		  (isPackaged,w) = electron .# "app.isPackaged" .?? (False, w)
		# (process,w) = (jsGlobal "require" .$ "process") w
		  (argv,w) = process .# "argv" .? w
		  (argv,w) = jsValToList` argv (fromJS "") w
		-> (if isPackaged ["":argv] argv, w)

openURLExternally :: !String -> Task ()
openURLExternally url = runInMainProcess open
where
	open me w
		# (electron,w) = (jsGlobal "require" .$ "electron") w
		# w = (electron .# "shell.openExternal" .$! url) w
		= w
