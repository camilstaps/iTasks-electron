implementation module Electron.Dialog

/**
 * This file is part of iTasks-Electron.
 *
 * iTasks-Electron is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * iTasks-Electron is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with iTasks-Electron. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import StdEnv

import Data.Functor

import ABC.Interpreter.JavaScript

import iTasks

import Electron.App

instance toString MessageType
where
	toString InfoMsg     = "info"
	toString ErrorMsg    = "error"
	toString QuestionMsg = "question"
	toString WarningMsg  = "warning"

showMessageBox :: !MessageType !String !String ![String] !Int -> Task (?String)
showMessageBox type title message buttons default =
	runInMainProcessWithResult dialog @ fmap ((!!) buttons)
where
	dialog _ world
		# (electron,world) = (jsGlobal "require" .$ "electron") world
		# (r,world) = (electron .# "dialog" .# "showMessageBoxSync" .$ jsRecord
			[ "title"     :> title
			, "message"   :> message
			, "type"      :> toString type
			, "buttons"   :> buttons
			, "normalizeAccessKeys" :> True
			, "defaultId" :> default
			]) world
		= (jsValToInt r, world)

showSimpleMessageBox :: !MessageType !String -> Task ()
showSimpleMessageBox type message =
	showMessageBox type (capitalize (toString type)) message ["OK"] 0 @!
	()
where
	capitalize s = s:=(0, toUpper s.[0])

defaultOpenDialogSettings :: OpenDialogSettings
defaultOpenDialogSettings =
	{ allowFiles = True
	, allowDirectories = False
	, showHiddenFiles = False
	, multiSelections = False
	, filters = []
	, defaultPath = ?None
	}

showOpenDialog :: !String !OpenDialogSettings -> Task (?String)
showOpenDialog title settings = runInMainProcessWithResult dialog
where
	dialog _ world
		# (dialog,world) = jsCall (jsGlobal "require") "electron" .# "dialog" .? world
		# (r,world) = (dialog .# "showOpenDialogSync" .$ jsRecord
			[ "title" :> title
			, "defaultPath" :> maybe (jsGlobal "undefined") toJS settings.OpenDialogSettings.defaultPath
			, "filters" :>
				[ jsRecord ["name" :> name, "extensions" :> exts]
				\\ (name, exts) <- settings.OpenDialogSettings.filters
				]
			, "properties" :> catMaybes
				[ if settings.allowFiles (?Just "openFile") ?None
				, if settings.allowDirectories (?Just "openDirectory") ?None
				, if settings.OpenDialogSettings.showHiddenFiles (?Just "showHiddenFiles") ?None
				, if settings.multiSelections (?Just "multiSelections") ?None
				]
			]) world
		| jsIsUndefined r
			= (?None, world)
		# (r,world) = r .# 0 .? world
		= (jsValToString r, world)

defaultSaveDialogSettings :: SaveDialogSettings
defaultSaveDialogSettings =
	{ SaveDialogSettings
	| showHiddenFiles = False
	, filters = []
	, defaultPath = ?None
	}

showSaveDialog :: !String !SaveDialogSettings -> Task (?String)
showSaveDialog title settings = runInMainProcessWithResult dialog
where
	dialog _ world
		# (dialog,world) = jsCall (jsGlobal "require") "electron" .# "dialog" .? world
		# (r,world) = (dialog .# "showSaveDialogSync" .$ jsRecord
			[ "title" :> title
			, "defaultPath" :> maybe (jsGlobal "undefined") toJS settings.SaveDialogSettings.defaultPath
			, "filters" :>
				[ jsRecord ["name" :> name, "extensions" :> exts]
				\\ (name, exts) <- settings.SaveDialogSettings.filters
				]
			, "properties" :> catMaybes
				[ if settings.SaveDialogSettings.showHiddenFiles (?Just "showHiddenFiles") ?None
				]
			]) world
		| jsIsUndefined r
			= (?None, world)
			= (jsValToString r, world)
