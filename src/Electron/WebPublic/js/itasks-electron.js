/**
 * This file is part of iTasks-Electron.
 *
 * iTasks-Electron is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * iTasks-Electron is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with iTasks-Electron. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

const fs=require ('fs').promises;
const net=require ('net');
const path=require ('path');
const process=require ('process');
const spawn=require ('child_process').spawn;
const {app}=require ('electron');

/* This module is assumed to be called from the iTasks -www directory. This
 * directory also contains abc-interpreter.js, so we can do a relative import.
 * Because path.join does not keep `./`, we use path.sep instead. */
const ABC_INTERPRETER=require ('.'+path.sep+'abc-interpreter.js');
const {ABCInterpreter,SharedCleanValue,CleanHeapValue}=ABC_INTERPRETER;

var log_server_close=false;

class Component {
	static instantiate (opts) {
		return ABCInterpreter.instantiate ({
			bytecode_path: opts.app+'.pbc',
			heap_size: 20<<20,
			stack_size: 500<<10,
			with_js_ffi: true,
			encoding: 'utf-8',
			fetch: path => {
				if (path.indexOf('/js/')==0)
					path='./'+opts.app+'-www'+path;

				return fs.readFile (path).then (buffer => ({
					ok: true,
					arrayBuffer: () => buffer.buffer
				}));
			},
		}).then (abc => {
			const socket=net.createConnection (opts.port,opts.host,() => {
				socket.write (JSON.stringify ({type: 'new', val: opts.new_arg || ''}) + '\n');
			});

			const component=new Component (abc,socket);
			if (typeof opts.debug!='undefined')
				component.do_debug=opts.debug;

			abc.component=component;
			return component;
		});
	}

	constructor (abc,socket) {
		this.abc=abc;
		this.socket=socket;
		this.shared_clean_values=null;
		this.do_debug=false;

		this.windows=new Map();

		this.buffer='';
		this.socket.on ('connect',() => {
			log_server_close=true;
		});
		this.socket.on ('data',this.onData.bind (this));
		this.socket.on ('close',() => {
			if (log_server_close)
				console.log ('server exited');
		});
		this.socket.on ('error',err => {
			if (err.code=='ECONNREFUSED')
				setTimeout (() => this.socket.connect (err.port,err.address), 100);
			else
				console.log ('connection error:',err.message);
		});
	}

	debug () {
		if (this.do_debug)
			console.log.apply (null,arguments);
	}

	tearDown () {
		this.send ('close','');
	}

	onData (data) {
		data=this.buffer+data.toString();
		const lines=data.split ('\n');
		this.buffer=lines.pop();

		lines.forEach (data => {
			data=JSON.parse (data);
			this.debug ('<--',data);

			switch (data.type){
				case 'run':
					this.run (data.val);
					break;
				default:
					throw new Error ('unknown message type "'+data.type+'"');
			}
		});
	}

	send (type,val,extra) {
		const json={
			type: type,
			val: val
		};

		if (typeof extra!='undefined')
			for (var k in extra)
				json[k]=extra[k];

		this.debug ('-->',json);
		this.socket.write (JSON.stringify (json) + '\n');
	}

	/* fun is a base64-encoded serialized Clean function wrapped with
	 * wrapInitFun */
	run (fun) {
		const val=this.abc.deserialize (Buffer.from (fun,'base64'));
		const ref=this.abc.share_clean_value (val,this);
		const args=[this, this.abc.initialized ? 0 : 1];
		this.abc.interpret (new SharedCleanValue (ref),args);
	}

	/* fun is a serialized Clean function */
	runOnServer (fun) {
		this.send ('run',fun);
	}
}

const DEFAULTS={
	host: 'localhost',
	itasks_port: 8080,
	port: 1234,
	debug: false
};

/* Pick a random TCP port */
const temp_server=net.createServer();
const temp_server_p=new Promise ((resolve,reject) => {
	temp_server.listen (0,() => {
		DEFAULTS.port=temp_server.address().port;
		temp_server.close(() => resolve());
	});
});

const temp_server_2=net.createServer();
const temp_server_p_2=new Promise ((resolve,reject) => {
	temp_server_2.listen (0,() => {
		DEFAULTS.itasks_port=temp_server_2.address().port;
		temp_server_2.close(() => resolve());
	});
});

const wait_for_tcp_ports=Promise.all ([temp_server_p,temp_server_p_2]);

function run (_opts) {
	return wait_for_tcp_ports.then(() => {
		const opts=DEFAULTS;
		Object.assign (opts,_opts);

		const exe='./'+opts.app+'.exe';
		const args=['serve','--electron-port',opts.port,'--port',opts.itasks_port];
		if (process.argv.indexOf ('--debug')>=0){
			opts.debug=true;
			console.log ('Run one of the following in a separate window:');
			console.log (exe+' '+args.join (' '));
			console.log ('gdb '+exe+' -ex \'r '+args.join (' ')+'\'');
			console.log ('valgrind '+exe+' '+args.join (' '));
		} else {
			const server=spawn (exe,args,{stdio: 'inherit'});
			server.on ('close',(code,signal) => {
				if (code!==0 && code!==null)
					console.log ('iTasks exited with exit code '+code);
				if (signal!==null){
					console.log ('iTasks exited with signal '+signal);
					app.exit (1);
				}
				app.exit (code);
			});
			app.on ('quit',() => server.kill());
		}

		return Component.instantiate (opts);
	});
}

module.exports={
	run: run
};
