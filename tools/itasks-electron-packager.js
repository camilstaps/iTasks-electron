/**
 * This file is part of iTasks-Electron.
 *
 * iTasks-Electron is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * iTasks-Electron is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with iTasks-Electron. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

const fs=require ('fs');
const path=require ('path');

const copydir=require ('copy-dir');
const packager=require ('electron-packager');

module.exports=function (_opts) {
	const opts={
		dir: '.',
		out: _opts.out,
		overwrite: true,
		executableName: _opts.electronApp,
		ignore: [
			/\.([di]cl|prj(\.default)?|prp)$/,
			/^\/WebPublic$/,
			/^\/iTasks-electron$/,
			/^\/Clean System Files$/,
			/^\/build.js$/,
		],
		afterCopy: [
			(buildPath,version,platform,arch,done) => {
				/* -www directory is expected at the root */
				copydir.sync (
					path.join (buildPath,_opts.cleanApp+'-www'),
					path.join (buildPath,'..','..',_opts.cleanApp+'-www')
				);

				/* Server app is expected at the root */
				const exts=['exe','bc','pbc'];
				for (i in exts)
					fs.renameSync (
						path.join (buildPath,_opts.cleanApp+'.'+exts[i]),
						path.join (buildPath,'..','..',_opts.cleanApp+'.'+exts[i])
					);

				if ('afterCopy' in _opts)
					_opts.afterCopy (buildPath,version,platform,arch,done);
				else
					done();
			}
		]
	};

	if ('ignore' in _opts)
		opts.ignore=opts.ignore.concat (_opts.ignore);

	return packager (opts);
}
